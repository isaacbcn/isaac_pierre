/**
 * Mouse Write. 
 Mouse click + over square program and writes the status to the serial port. 
 * With the Wiring / Arduino program that follows below.
 */
import processing.serial.*;

Serial myPort;  // Create object from Serial class

void setup() 
{
  size(1280, 200);

 
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);
}

void draw() {
  background(0);
  if (mouseButton == RIGHT){
    fill(0);
    myPort.write('D');
  }
  else if (mouseOverRect() == true) {  // If mouse is over square,
    fill(0,0,204);                    // change color and
    myPort.write('H'); // send an H to indicate mouse is over square
    rect(0, 0, 200, 200);       // Draw a square 
  } 
  else {                        // If mouse is not over square,
    fill(120,120,0);                      // change color and
    myPort.write('L');              // send an L otherwise
     rect(400, 0, 200, 200);
  }
if (mouseButton == LEFT) {
    fill(120,120,0);
     rect(400, 0, 200, 200);
    fill(255,0,0);
     rect(200, 0, 200, 200);
     rect(600, 0, 200, 200); 
        myPort.write('R');
  } 
  }
boolean mouseOverRect() { // Test if mouse is over square
  return ((mouseX >= 0) && (mouseX <= 200) && (mouseY >= 0) && (mouseY <= 200));
}


/*
  // Wiring/Arduino code:
 // Read data from the serial and turn ON or OFF a light depending on the value
 
 char val; // Data received from the serial port
 int blueledPin = 4; // Set the pin to digital I/O 4
 int greenledPin = 3; //set the pin to digitla I/O 3
 void setup() {
 pinMode(4, OUTPUT); // Set pin4 as OUTPUT
  pinMode(3, OUTPUT); // Set pin3 as OUTPUT
 Serial.begin(9600); // Start serial communication at 9600 bps
 }
 
 void loop() {
 while (Serial.available()) { // If data is available to read,
 val = Serial.read(); // read it and store it in val
 }
 if (val == 'H') { // If H was received
 digitalWrite(4, HIGH); // turn the LED on
 } else {
 digitalWrite(4, LOW); // Otherwise turn it OFF
 }
 if (val == 'L') {
  digitalWrite(3, HIGH);
 } else{
  digitalWrite(3, LOW);
 }
 delay(100); // Wait 100 milliseconds for next reading
 }
 
 */