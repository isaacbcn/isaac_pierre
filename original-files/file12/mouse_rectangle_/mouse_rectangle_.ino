 char val; // Data received from the serial port
 int blueledPin = 4; // Set the pin to digital I/O 4
 int greenledPin = 3; //set the pin to digitla I/O 3
 void setup() {
 pinMode(4, OUTPUT); // Set pin4 as OUTPUT
 pinMode(3, OUTPUT); // Set pin3 as OUTPUT
 pinMode(5, OUTPUT); // Set pin5 as OUTPUT
 Serial.begin(9600); // Start serial communication at 9600 bps
 }
 
 void loop() {
 while (Serial.available()) { // If data is available to read,
 val = Serial.read(); // read it and store it in val
 }
 if (val == 'H') { // If H was received
 digitalWrite(4, HIGH); // turn the LED on
 } else {
 digitalWrite(4, LOW); // Otherwise turn it OFF
 }
 if (val == 'L') {
  digitalWrite(3, HIGH);
 } else{
  digitalWrite(3, LOW);
 }
 if (val == 'D') {
  digitalWrite(4, LOW);
  digitalWrite(3, LOW);
  digitalWrite(5, LOW);
 }
 if (val == 'R') {
  digitalWrite(4, LOW);
  digitalWrite(3, LOW);
  digitalWrite(5, HIGH);
 }
 delay(100); // Wait 100 milliseconds for next reading
 }
 
