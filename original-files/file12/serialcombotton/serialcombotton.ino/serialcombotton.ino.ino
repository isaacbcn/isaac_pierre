//serial communication button 

// Code for sensing a switch status and writing the value to the serial port.

int button = 4;                       // Switch connected to pin 4

void setup() {
  pinMode(button, INPUT);             // Set pin 4 as an input
  Serial.begin(9600);                    // Start serial communication at 9600 bps
}

void loop() {
  if (digitalRead(button) == HIGH) {  // If button is pressed,
    Serial.write(1);               // send 1 to Processing
  } else {                               // If button not pressed,
    Serial.write(0);               // send 0 to Processing
  }
  delay(100);                            // Wait 100 milliseconds
}

