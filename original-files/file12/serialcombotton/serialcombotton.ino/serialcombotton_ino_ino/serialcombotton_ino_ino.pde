/**
 * Read data from the serial port and change the color of a rectangle
 * when a switch connected to a Arduino board is pressed and released.
 */
import processing.serial.*;
Serial myPort;  // Create object from Serial class
int val;      // Data received from the serial port
color c1 = color(255, 204, 0);

void setup() {
  size(300, 500); //set the size of the interface window 
 println(Serial.list());//lists the ports in the serial list on my mac
  String portName = Serial.list()[1];  // assign the port arduino is using for serial comunication.
  //0=first port on the port list, 1=second port.... etc. etc
  myPort = new Serial(this, portName, 9600);// set the class for the port, port name and baudrate
}

void draw()//draw is to processing what loop is to Arduino
{
  if ( myPort.available() > 0) {  // If data is available,
    val = myPort.read();         // read it and store it in val
  }
  background(255);             // Set background to white
  if (val == 0) {              // If the serial value is 0,
    fill(0);                   // set fill to black
  } 
  else {                       // If the serial value is not 0,
    fill(c1);                 // set fill to light gray
  }
  rect(10, 10, 280, 480);    // set the position and the dimension of the rectangle to be drawn
}



/*

// Wiring / Arduino Code
// Code for sensing a switch status and writing the value to the serial port.

int switchPin = 4;                       // Switch connected to pin 4

void setup() {
  pinMode(switchPin, INPUT);             // Set pin 0 as an input
  Serial.begin(9600);                    // Start serial communication at 9600 bps
}

void loop() {
  if (digitalRead(switchPin) == HIGH) {  // If switch is ON,
    Serial.write(1);               // send 1 to Processing
  } else {                               // If the switch is not ON,
    Serial.write(0);               // send 0 to Processing
  }
  delay(100);                            // Wait 100 milliseconds
}

*/
