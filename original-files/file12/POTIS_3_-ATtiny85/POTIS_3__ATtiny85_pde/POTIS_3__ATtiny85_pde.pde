import processing.serial.*;
Serial myPort;

int value;
int padding = 100;
float m;
String myString;
float val;
int[] pin_val;
int lf = 10;
boolean received;
float reading = 0;

void setup() {
  size(1200, 200);
  //  println("Available serial ports:");
   println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);
  
  smooth();
  strokeWeight(10);
  stroke(10);
}

void draw() {
  background(#FFFFF0);
  line(padding, height/2, width-padding, height/2);
  noStroke();
  int g = int(reading*255.0);
  fill(0, g, 0);
  float x = reading*1023.0 +100;
  // todo: translate reading in the range 0 to 1 to x that has a value between 100 and 1123
 println("drawing ellipse at x=" + x);
  
  //todo: we should change the colors so it varies depending on inByte whcih is between
  //0 and 200
  ellipse(x, height/2, 100, 100); 
  stroke(10);
}

void serialEvent (Serial myPort)
 {
String inString = myPort.readStringUntil('\n');
 
if (inString!= null) {
 
// trim off any whitespace;
 
inString = trim(inString);
 
//convert to an int and map to the screen height:
//convert 0 to 1023 to 0 to 1.0

reading = float(inString)/1023.0;

println("normalized arduino value: " + reading);
//reading = map(reading, 0, 255, 0, height);
 
}
}
