import processing.serial.*; 
Serial myPort;
int value; // integrer value
int padding = 100; //integrer of the padding of the travel line
float m; // declare a float variable -convert a string into a numeric value with decimal
String myString; //declare a sequence of characters called my string
float val; 
int[] pin_val;// array access
int lf = 10; // line feed in ASCII
boolean received;//
float inByte = 0; //the byte representation of the primitive datatype coming in and converted into a numerical value with decimal is equal to 0

void setup() {
  size(1200, 200);
  //  println("Available serial ports:");
   println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);
  smooth();
  strokeWeight(10);
  stroke(10);
}
void draw() {
  background(#FFFFF0);
  line(padding, height/2, width-padding, height/2);
  noStroke();
  fill(0, 120, 0);
  ellipse(inByte, height/2, 100, 100); 
  stroke(10);
}
void serialEvent (Serial myPort) //gets caled everytime there is a carriage return
 {
String inString = myPort.readStringUntil('\n');//put the incoming data into a string-
//the ('\n')is a delimiter indicating the end of a complete packet
if (inString!= null) { //make shure data is not empty before continuing
inString = trim(inString);// trim off any whitespace; 
inByte = float(inString)+100;
println(inByte); 
inByte = map(inByte, 0, 200, 0, height);//convert to an int and map to the screen height:
}
}