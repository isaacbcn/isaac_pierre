
/* Analog Read potis + phototrnasistor

by Isaac Pierre 
fab academy 2017_2018
*/
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0,A1);
byte TransPin = A2;    // select the input pin for the Photoresistor (PR)
//byte PotPin = A3;    // select the input pin for the potentiometer

int val= 0; // variable to store the value coming from the sensor 
int valTrans = 0; //variable to store the value from the Pr sensor
int valPot = 0; //variable to store the value from the Potiometer

  #define BAUD (9600)
  
void setup() {
mySerial.begin(9600);

pinMode (TransPin, INPUT); // declare the PR Pin/A2 as an INTPUT
//pinMode (PotPin, INPUT);  // declare the potentiometer Pin/A3 as an INTPUT
}

void loop() {
valTrans = analogRead(TransPin);    // read the value from the PR sensor
//valPot = analogRead(PotPin); // read the value from the potentiometer
mySerial.println(valTrans);
 //mySerial.write(valTrans); //print the value of the PR sensor on computer screen
 //mySerial.println(valPot); //print the value of the potentiometer on the computer screen
 delay(100);                  // stop the program for some time
//digitalWrite(ledPin, LOW);   // turn the ledPin off
 //delay(val);                  // stop the program for some time
 }
