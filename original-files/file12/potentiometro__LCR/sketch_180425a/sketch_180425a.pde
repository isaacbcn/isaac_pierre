import processing.serial.*;
Serial myPort;
PFont myFont;
float val;
int[] pin_val;
int lf = 10;
boolean received;
float inByte = 0;
 
void setup() {      
 
size(325,400);
 
fill(51,153,255);
 
 
rect(10,10,300,150);
 
myFont = createFont("Arial", 18);
 
textFont(myFont, 18);
 
println(Serial.list());
 
myPort = new Serial (this, Serial.list()[1], 9600);
 
pin_val = new int[0];
 
val = 0;
 
received = false;
 
}
 
void draw()
 
{
textSize(30);
 
fill(255,0,0);
 
textAlign(CENTER);
 
textFont(myFont, 18);
 
text("received:   "+inByte, 160,50 );                                                             //This is where I believe my fault is.
fill(255,255,255);
 
ellipse(162.5,260,80,80);
}
 
void serialEvent (Serial myPort)
 {
String inString = myPort.readStringUntil('\n');
 
if (inString!= null) {
 
// trim off any whitespace;
 
inString = trim(inString);
 
//convert to an int and map to the screen height:
 
inbyte = float(inString);
 
println(inByte);
 
inByte = map(inByte, 0, 1023, 0, height);
 
}
}
