import processing.serial.*;
Serial myPort;
int value;
int padding = 100;
float m;
String myString;
float val;
int[] pin_val;
int lf = 10;
boolean received;
float inByte = 0;

void setup() {
  size(1200, 200);
  //  println("Available serial ports:");
   println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);
  smooth();
  strokeWeight(10);
  stroke(10);
}
void draw() {
  background(#FFFFF0);
  line(padding, height/2, width-padding, height/2);
  noStroke();
  fill(0, 120, 0);
  ellipse(inByte, height/2, 100, 100); 
  stroke(10);
}
void serialEvent (Serial myPort)
 {
String inString = myPort.readStringUntil('\n');
if (inString!= null) { 
inString = trim(inString);// trim off any whitespace; 
inByte = float(inString)+100;//convert to an int and map to the screen height:
println(inByte);
inByte = map(inByte, 0, 200, 0, height);
}
}
