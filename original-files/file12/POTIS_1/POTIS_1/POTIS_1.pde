import processing.serial.*;
Serial myPort;

int value;
int padding = 100;
float m;

void setup() {
  size(1200, 210);
  //  println("Available serial ports:");
  //  println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);
  smooth();
  strokeWeight(10);
  stroke(100);
}

void draw() {
  background(#FFFFF0);
  if (myPort.available() > 0) {
    value = myPort.read();
    println(value); 
    m = map(value, 0, 255, padding, width-padding);
  } 
  line(padding, height/2, width-padding, height/2);
  noStroke();
  fill(#FFDE14);
  ellipse(m, height/2, 150, 150); 
  stroke(100);
}
