import processing.serial.*;

// interface assignment
//graph example 
//this program creates a graph from Light sensor datas

//created 24 april 2018
//by Isaac Pierre Racine 
//adapted from original code by Tom Igoe

import processing.serial.*; // import processing library

Serial myPort; // serial port
int xPos = 1; //horizontal position of the graph
float inByte = 0;
void setup () { 
  size (555, 400);  //set the window size
  println(Serial.list());  //lists all the available ports
  myPort = new Serial(this, Serial.list()[1], 9600); //Arduino is connected to 2nd port of the list
  myPort.bufferUntil('\n'); //do not generate a new SerialEvent unless you get a newline character:
  background(0);   //set initial background:
}
void draw () {
 stroke(127, 34, 255);
  line(xPos, height, xPos, height - inByte);
  if (xPos >= width) { //a the edge of the screen, go back to the beginning:
    xPos = 0;  
    background(0); // background color black
  } else {
    xPos++; //increment the horizontal position:
  }
}
void serialEvent (Serial myPort)   //everything happens in the serialEvent()
{
  String inString = myPort.readStringUntil('\n');  //get the ASCII string:
  if (inString != null) {
    inString = trim(inString);     //trim off any whitespace: 
    inByte = float(inString); 
    println(inByte);
    inByte = map(inByte, 450, 900, 0, height);  //convert to an int and map to the screen height:
   
 
  }
}
