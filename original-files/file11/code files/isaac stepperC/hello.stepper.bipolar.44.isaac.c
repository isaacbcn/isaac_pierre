//
//
// hello.stepper.bipolar.44.isaac.c
//
// bipolar full stepping hello-world
//
// Neil Gershenfeld
// 11/21/12
//
// (c) Massachusetts Institute of Technology 2012
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose. Copyright is
// retained and must be preserved. The work is provided
// as is; no warranty is provided, and users accept all
// liability.
//

#include <avr/io.h>
#include <util/delay.h>

#define output(directions,pin) (directions |= pin) // set port direction for output
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin
#define pin_test(pins,pin) (pins & pin) // test for port pin
#define bit_test(byte,bit) (byte & (1 << bit)) // test for bit set

#define bridge_port PORTA // H-bridge port
#define bridge_direction DDRA // H-bridge direction
#define A2 (1 << PA0) // H-bridge output pins
#define A1 (1 << PA1) // "
#define B2 (1 << PA3) // "
#define B1 (1 << PA4) // "
#define on_delay() _delay_us(25) // PWM on time
#define off_delay() _delay_us(5) // PWM off time
#define PWM_count 100 // number of PWM cycles
#define step_count 20 // number of steps

static uint8_t count;

//
// A+ B+ PWM pulse
//
void pulse_ApBp() {
  clear(bridge_port, A2);
  clear(bridge_port, B2);
  set(bridge_port, A1);
  set(bridge_port, B1);
   for (count = 0; count < PWM_count; ++count) {
      set(bridge_port, A1);
      set(bridge_port, B1);
      on_delay();
      clear(bridge_port, A1);
      clear(bridge_port, B1);
      off_delay();
      }
   }
//
// A+ B- PWM pulse
//
void pulse_ApBm() {
  clear(bridge_port, A2);
  clear(bridge_port, B1);
  set(bridge_port, A1);
  set(bridge_port, B2);
   for (count = 0; count < PWM_count; ++count) {
      set(bridge_port, A1);
      set(bridge_port, B2);
      on_delay();
      clear(bridge_port, A1);
      clear(bridge_port, B2);
      off_delay();
      }
   }
//
// A- B+ PWM pulse
//
void pulse_AmBp() {
  clear(bridge_port, A1);
  clear(bridge_port, B2);
  set(bridge_port, A2);
  set(bridge_port, B1);
   for (count = 0; count < PWM_count; ++count) {
      set(bridge_port, A2);
      set(bridge_port, B1);
      on_delay();
      clear(bridge_port, A2);
      clear(bridge_port, B1);
      off_delay();
      }
   }
//
// A- B- PWM pulse
//
void pulse_AmBm() {
  clear(bridge_port, A1);
  clear(bridge_port, B1);
  set(bridge_port, A2);
  set(bridge_port, B2);
   for (count = 0; count < PWM_count; ++count) {
      set(bridge_port, A2);
      set(bridge_port, B2);
      on_delay();
      clear(bridge_port, A2);
      clear(bridge_port, B2);
      off_delay();
      }
   }
//
// clockwise step
//there is a bug if you call it repeatedly eventually it starts going back and frth instead of vcontinuiing going round
void step_cw() {
   pulse_ApBp();
   pulse_AmBp();
   pulse_AmBm();
   pulse_ApBm();
   }
//
// counter-clockwise step
//there is a possible bug if you call it repeatedly eventually it starts going back and frth instead of vcontinuiing going round.
//could be connection  in the electronics or miswiring ......
void step_ccw() {
   pulse_ApBm();
   pulse_AmBm();
   pulse_AmBp();
   pulse_ApBp();
   }
// we want:
// 1. start n at 0
// 2. repaet the following sequence but incremet the number of steps n by 50 each time:
//   2a. the sequence is to spin n steps cw
//   2b. than n steps step_ccw

//spins n steps clockwise
void step_ncw(int n) {
int i=0;
  while(i<n){
    i++;
  step_cw();
  }
}
  //spins n steps counter clockwise
void step_nccw(int n) {
  int i=0;
    while(i<n ){
      i++;
    step_ccw();
  }
}

int main(void) {
   //
   // main
   //
   static uint8_t i,j;
   //
   // set clock divider to /1
   //
   CLKPR = (1 << CLKPCE);
   CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);
   //
   // initialize bridge pins
   //
   clear(bridge_port, A1);
   output(bridge_direction, A1);
   clear(bridge_port, A2);
   output(bridge_direction, A2);
   clear(bridge_port, B1);
   output(bridge_direction, B1);
   clear(bridge_port, B2);
   output(bridge_direction, B2);


   // we want:
   // 1. n starts at 0
   int n=0;
   // 2. repaet the following sequence for ever but incremet the number of steps n by 50 each time:
   while (1){
   n= n+5;
   // 2a. the sequence is to spin n steps cw
   step_ncw(n);
   //   2b. than n steps step_ccw
   step_nccw(n);
}


  // i++ or ++i both mean: i = i + 1
   //for (int i = 0; i < 3; i++) {
    // printf("hello from number " + i);
     // "hello from number 0"
     // "hello from number 1"
     // "hello from number 2"
   //}


   //
   // main loop
   //loop are convenient to repeat sets of instructions

//in cc there are several types of loop. for now while and for
//while (operator) loop continue running the instructions inside the body while the condition inside the parentesis are true
//in c number can have true or false value- o santds for false and any other number stands for true
//while (i<500) {
//i++;
//if (i<200)
//step_cw();
//else
//step_ccw();

//while (1){
//step_cw();

//}
    //   step_ccw();
     //the way a for loop works is that you have 3 parts seperated by semicolons and the 1st part id the initialization
     //and that means run the code int here before entering the loop
     //set the variable i to zero, run the loop and after one run evaluate the secod part and figure if it is true or false

/*    for (i = 0; i < step_count; ++i) {
         for (j = 0; j < i; ++j)
            step_cw();
         for (j = 0; j < i; ++j)
            step_ccw();
         }
      for (i = step_count; i > 0; --i) {
         for (j = 0; j < i; ++j)
            step_cw();
         for (j = 0; j < i; ++j)
            step_ccw();
         }*/
  //    }
}
