Last login: Wed Mar  7 16:24:52 on ttys000
ISAAC-LAPTOP:~ isaacpierreracine$ cd ~/Desktop/FA2017/originals/week8/xfiles
-bash: cd: /Users/isaacpierreracine/Desktop/FA2017/originals/week8/xfiles: No such file or directory
ISAAC-LAPTOP:~ isaacpierreracine$ /Users/isaacpierreracine/Desktop/FA2017/originals/week8/Xfile 
-bash: /Users/isaacpierreracine/Desktop/FA2017/originals/week8/Xfile: is a directory
ISAAC-LAPTOP:~ isaacpierreracine$ make
make: *** No targets specified and no makefile found.  Stop.
ISAAC-LAPTOP:~ isaacpierreracine$ make /f
make: *** No rule to make target `/f'.  Stop.
ISAAC-LAPTOP:~ isaacpierreracine$ make -f hello.ftdi.44.echo.c.make
make: hello.ftdi.44.echo.c.make: No such file or directory
make: *** No rule to make target `hello.ftdi.44.echo.c.make'.  Stop.
ISAAC-LAPTOP:~ isaacpierreracine$ /Users/isaacpierreracine/Desktop/FA2017/originals/week8/Xfile/hello.ftdi.44.echo.c.make 
-bash: /Users/isaacpierreracine/Desktop/FA2017/originals/week8/Xfile/hello.ftdi.44.echo.c.make: Permission denied
ISAAC-LAPTOP:~ isaacpierreracine$ cd /Users/isaacpierreracine/Desktop/FA2017/originals/week8/Xfile 
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls
hello.ftdi.44.echo.c		hello.ftdi.44.echo.c.make
ISAAC-LAPTOP:Xfile isaacpierreracine$ make -f hello.ftdi.44.echo.c
hello.ftdi.44.echo.c:1: *** missing separator.  Stop.
ISAAC-LAPTOP:Xfile isaacpierreracine$ make -f hello.ftdi.44.echo.c.make
avr-gcc -mmcu=attiny44 -Wall -Os -DF_CPU=20000000 -I./ -o hello.ftdi.44.echo.out hello.ftdi.44.echo.c
avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
	avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
AVR Memory Usage
----------------
Device: attiny44

Program:     758 bytes (18.5% Full)
(.text + .data + .bootloader)

Data:         64 bytes (25.0% Full)
(.data + .bss + .noinit)


ISAAC-LAPTOP:Xfile isaacpierreracine$ make -f hello.ftdi.44.echo.c.make program-usbtiny
avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
	avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
AVR Memory Usage
----------------
Device: attiny44

Program:     758 bytes (18.5% Full)
(.text + .data + .bootloader)

Data:         64 bytes (25.0% Full)
(.data + .bss + .noinit)


avrdude -p t44 -P usb -c usbtiny -U flash:w:hello.ftdi.44.echo.c.hex

avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.01s

avrdude: Device signature = 0x1e9207
avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
         To disable this feature, specify the -D option.
avrdude: erasing chip
avrdude: reading input file "hello.ftdi.44.echo.c.hex"
avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
avrdude: writing flash (758 bytes):

Writing | ################################################## | 100% 1.18s

avrdude: 758 bytes of flash written
avrdude: verifying flash memory against hello.ftdi.44.echo.c.hex:
avrdude: load data flash data from input file hello.ftdi.44.echo.c.hex:
avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
avrdude: input file hello.ftdi.44.echo.c.hex contains 758 bytes
avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 1.04s

avrdude: verifying ...
avrdude: 758 bytes of flash verified

avrdude: safemode: Fuses OK (H:FF, E:DF, L:FE)

avrdude done.  Thank you.

ISAAC-LAPTOP:Xfile isaacpierreracine$ hello
-bash: hello: command not found
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls
hello.ftdi.44.echo.c		hello.ftdi.44.echo.c.make
hello.ftdi.44.echo.c.hex	hello.ftdi.44.echo.out
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls
hello.ftdi.44.echo.c		hello.ftdi.44.echo.c.make	term.py
hello.ftdi.44.echo.c.hex	hello.ftdi.44.echo.out
ISAAC-LAPTOP:Xfile isaacpierreracine$ sudo python term.py /dev/ttyUSB0 115200
Password:
Sorry, try again.
Password:
Sorry, try again.
Password:
Traceback (most recent call last):
  File "term.py", line 92, in <module>
    ser = serial.Serial(port,speed)
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/serial/serialutil.py", line 240, in __init__
    self.open()
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/serial/serialposix.py", line 268, in open
    raise SerialException(msg.errno, "could not open port {}: {}".format(self._port, msg))
serial.serialutil.SerialException: [Errno 2] could not open port /dev/ttyUSB0: [Errno 2] No such file or directory: '/dev/ttyUSB0'
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls -la  /dev/ttyUSB0 
ls: /dev/ttyUSB0: No such file or directory
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls -la  /dev/tty
Display all 132 possibilities? (y or n)
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls -la  /dev/tty
Display all 132 possibilities? (y or n)
tty                          ttyqe                        ttyse                        ttyuf
tty.Bluetooth-Incoming-Port  ttyqf                        ttysf                        ttyv0
tty.usbserial-FT9KZC5L       ttyr0                        ttyt0                        ttyv1
ttyp0                        ttyr1                        ttyt1                        ttyv2
ttyp1                        ttyr2                        ttyt2                        ttyv3
ttyp2                        ttyr3                        ttyt3                        ttyv4
ttyp3                        ttyr4                        ttyt4                        ttyv5
ttyp4                        ttyr5                        ttyt5                        ttyv6
ttyp5                        ttyr6                        ttyt6                        ttyv7
ttyp6                        ttyr7                        ttyt7                        ttyv8
ttyp7                        ttyr8                        ttyt8                        ttyv9
ttyp8                        ttyr9                        ttyt9                        ttyva
ttyp9                        ttyra                        ttyta                        ttyvb
ttypa                        ttyrb                        ttytb                        ttyvc
ttypb                        ttyrc                        ttytc                        ttyvd
ttypc                        ttyrd                        ttytd                        ttyve
ttypd                        ttyre                        ttyte                        ttyvf
ttype                        ttyrf                        ttytf                        ttyw0
ttypf                        ttys0                        ttyu0                        ttyw1
ttyq0                        ttys000                      ttyu1                        ttyw2
ttyq1                        ttys1                        ttyu2                        ttyw3
ttyq2                        ttys2                        ttyu3                        ttyw4
ttyq3                        ttys3                        ttyu4                        ttyw5
ttyq4                        ttys4                        ttyu5                        ttyw6
ttyq5                        ttys5                        ttyu6                        ttyw7
ttyq6                        ttys6                        ttyu7                        ttyw8
ttyq7                        ttys7                        ttyu8                        ttyw9
ttyq8                        ttys8                        ttyu9                        ttywa
ttyq9                        ttys9                        ttyua                        ttywb
ttyqa                        ttysa                        ttyub                        ttywc
ttyqb                        ttysb                        ttyuc                        ttywd
ttyqc                        ttysc                        ttyud                        ttywe
ttyqd                        ttysd                        ttyue                        ttywf
ISAAC-LAPTOP:Xfile isaacpierreracine$ ls -la  /dev/tty.usbserial-FT9KZC5L 
crw-rw-rw-  1 root  wheel   18,   8 Mar 15 16:09 /dev/tty.usbserial-FT9KZC5L
ISAAC-LAPTOP:Xfile isaacpierreracine$ sudo python term.py /dev/tty.usbserial-FT9KZC5L 115200
^CTraceback (most recent call last):
  File "term.py", line 122, in <module>
    root.mainloop()
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk/Tkinter.py", line 1125, in mainloop
    self.tk.mainloop(n)
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk/Tkinter.py", line 1531, in __call__
    def __call__(self, *args):
KeyboardInterrupt
ISAAC-LAPTOP:Xfile isaacpierreracine$ sudo python term.py /dev/tty.usbserial-FT9KZC5L 115200
kkl^[[A^[[B^CException in Tkinter callback
Traceback (most recent call last):
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk/Tkinter.py", line 1536, in __call__
    return self.func(*args)
  File "/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk/Tkinter.py", line 587, in callit
    func(*args)
  File "term.py", line 78, in idle
    time.sleep(0.001)
KeyboardInterrupt
ISAAC-LAPTOP:Xfile isaacpierreracine$ python term.py /dev/tty.usbserial-FT9KZC5L 115200
g
