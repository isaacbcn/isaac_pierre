//
//
// hello.ftdi.44.echo.c
//
// 115200 baud FTDI character echo, with flash string
//
// set lfuse to 0x5E for 20 MHz xtal
//
// Neil Gershenfeld
// 12/8/10
//
// (c) Massachusetts Institute of Technology 2010
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose. Copyright is
// retained and must be preserved. The work is provided
// as is; no warranty is provided, and users accept all
// liability.
//

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define output(directions,pin) (directions |= pin) // set port direction for output
#define input(directions,pin) (directions &= (~pin)) // set port direction for output
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin
#define pin_test(pins,pin) (pins & pin) // test for port pin
#define bit_test(byte,bit) (byte & (1 << bit)) // test for bit set
#define bit_delay_time 8.5 // bit delay for 115200 with overhead
#define bit_delay() _delay_us(bit_delay_time) // RS232 bit delay
#define half_bit_delay() _delay_us(bit_delay_time/2) // RS232 half bit delay
#define char_delay() _delay_ms(10) // char delay

#define SERIAL_PIN_IN (1 << PA0)
#define SERIAL_PIN_OUT (1 << PA1)
#define LED_PIN (1 << PB2)
#define BUTTON_PIN (1 << PA7)

#define max_buffer 25

// read character into rxbyte on pins pin
//    assumes line driver (inverts bits)
void get_char(volatile unsigned char *pins, unsigned char pin, char *rxbyte) {
   *rxbyte = 0;
   while (pin_test(*pins,pin));

   // delay to middle of first data bit
   half_bit_delay();
   bit_delay();

   //unrolled loop, read data bits
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 0);
   else
      *rxbyte |= (0 << 0);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 1);
   else
      *rxbyte |= (0 << 1);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 2);
   else
      *rxbyte |= (0 << 2);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 3);
   else
      *rxbyte |= (0 << 3);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 4);
   else
      *rxbyte |= (0 << 4);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 5);
   else
      *rxbyte |= (0 << 5);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 6);
   else
      *rxbyte |= (0 << 6);
   bit_delay();
   if pin_test(*pins,pin)
      *rxbyte |= (1 << 7);
   else
      *rxbyte |= (0 << 7);

   // wait for stop bit
   bit_delay();
   half_bit_delay();
}

// send character in txchar on port pin
//    assumes line driver (inverts bits)
void put_char(volatile unsigned char *port, unsigned char pin, char txchar) {

   // start bit
   clear(*port,pin);
   bit_delay();

   // unrolled loop to write data bits
   if bit_test(txchar,0)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,1)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,2)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,3)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,4)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,5)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,6)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();
   if bit_test(txchar,7)
      set(*port,pin);
   else
      clear(*port,pin);
   bit_delay();

   // stop bit
   set(*port,pin);
   bit_delay();

   // char delay
   bit_delay();
}

// print a null-terminated string
void put_string(volatile unsigned char *port, unsigned char pin, char *str) {
   static int index;
   index = 0;
   do {
      put_char(port, pin, str[index]);
      ++index;
      } while (str[index] != 0);
   }

void set_clock_divider_to_1(){
  CLKPR = (1 << CLKPCE);
  CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);
}

void initialize_pins(){
  output(DDRA, SERIAL_PIN_OUT);
  set(PORTA, SERIAL_PIN_OUT);

  output(DDRB, LED_PIN);
  set(PORTB, LED_PIN);

  input(DDRA, BUTTON_PIN);
}

int main(void) {
  set_clock_divider_to_1();
  initialize_pins();
  put_string(&PORTA, SERIAL_PIN_OUT, "Starting serial press-button-counter test\n");

  static char button_str[max_buffer] = {0};
  static int times_button_pressed = 0;

  while (1) {
    //when button is pressed, input pin goes to ground
    loop_until_bit_is_clear(PINA, PINA7);
    clear(PORTB, LED_PIN);

    put_string(&PORTA, SERIAL_PIN_OUT, "press-button: ");
    times_button_pressed++;
    itoa(times_button_pressed, button_str, 10);
    put_string(&PORTA, SERIAL_PIN_OUT, button_str);
    put_string(&PORTA, SERIAL_PIN_OUT, " times\n");

    loop_until_bit_is_set(PINA, PINA7);
    set(PORTB, LED_PIN);
  }
}
