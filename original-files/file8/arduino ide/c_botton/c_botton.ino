int main(void)
{
// The "special function registers" used in this program are:
// DDRA -- Port A Data Direction Register (DDRB for port B)
// A high (1) value indicates output, low (0) is input.
// We set the 7th bit in DDRA to configure the PA7 pin as output pin.
// PORTA -- Port A Data Register
// This is used to set the output value for each of the eight port A pins.

   DDRB = 0b00000100; //Set PB2 (LED) as output pin
   PORTB = 0b00000000; //Set output pins in 0v

   DDRA = 0b01111111; //Set PA7 (Button) as input pin
   PORTA = 0b10000000; //Set PA7 pull-up resistor on

while(1) //Infinite loop
{
  if (bit_is_clear(PINA, PINA7)) //If button is pushed (PA7 == 1)
{
  PORTB = 0b00000100; // Turn ON the PB2 (LED)
  _delay_ms(1000); // 1000ms
  PORTB = 0b00000000; // Turn OFF the PB2 (LED)
  _delay_ms(100); // 100ms
}
else
{
  PORTB = 0b00000100; // Turn ON the PB2 (LED)
}
}
return 0;
}
