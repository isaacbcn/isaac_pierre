int LEDR = 0;     //analog pin to which LDR is connected, here we set it to 0 so it means A0
int LEDRValue = 0;      //that’s a variable to store LDR values
int light_sensitivity = 500;    //This is the approx value of light surrounding your LDR

void setup()
  {
    Serial.begin(9600);          //start the serial monitor with 9600 baud
    pinMode(13, OUTPUT);     //we mostly use 13 because there is already a built in yellow LED in arduino which shows output when 13 pin is enabled
    pinMode(12, OUTPUT);    // green led co¡nnected to pin 12
  }
 
void loop()
  {
    LEDRValue = analogRead(LEDR); //reads the ldr’s value through LDR 

    Serial.println(LEDRValue);       //prints the LDR values to serial monitor
    delay(500);        //This is the speed by which LDR sends value to arduino
 
    if (LEDRValue < light_sensitivity) // if vlaue of Red LED is less than light sensitivity
      {
        digitalWrite(13, HIGH);// turn red LED on
        delay(500); // wait 500 ms
        digitalWrite(13,LOW); // turn red LED off
      }
    else // otherwise
      {
        digitalWrite(12, LOW);//turn green LED off
      }
  if (LEDRValue > light_sensitivity)// if value of red LED is higher than light sensitivity
  {digitalWrite(12, HIGH);//turn green LED on
  delay(500);//wait 500ms
  digitalWrite(12,LOW);//turn green LED off
  }
  else //otherwise
  {digitalWrite(12,LOW);//turn the green LED off
  }
  }
