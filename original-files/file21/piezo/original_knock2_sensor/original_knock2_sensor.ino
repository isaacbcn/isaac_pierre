/* Knock Sensor

   This sketch reads a piezo element to detect a knocking sound.
   It reads an analog pin and compares the result to a set threshold.
   If the result is greater than the threshold, it writes
   "knock" to the serial port, and toggles the LED on pin 13.

   The circuit:
  * + connection of the piezo attached to analog in 0
  * - connection of the piezo attached to ground
  * 1-megohm resistor attached from analog in 0 to ground

   http://www.arduino.cc/en/Tutorial/Knock

   created 25 Mar 2007
   by David Cuartielles <http://www.0j0.org>
   modified 30 Aug 2011
   by Tom Igoe

   This example code is in the public domain.

 */


// these constants won't change:
const int ledPingreen = 9;      // led connected to digital pin 9
const int ledPinyellow = 10;      // yellowled connected to digital pin 9
//const int ledPinred = 8;      // red led connected to digital pin 8
const int knockSensor0 = A0; // the piezo is connected to analog pin 0
const int knockSensor1 = A1; // the piezo is connected to analog pin 0
const int threshold =100;  // threshold value to decide when the detected sound is a knock or not


// these variables will change:
int sensor0Reading = 0;      // variable to store the value read from the sensor pin
int ledgreenState = LOW;     // variable used to store the last GreenLED status, to toggle the light
int sensor1Reading = 0;      // variable to store the value read from the sensor pin
int ledyellowState = LOW;    // variable used to store the last yellow LED status, to toggle the light
void setup() {
  pinMode(10, OUTPUT); // declare the ledyellow Pin as as OUTPUT
  pinMode(9, OUTPUT); // declare the ledgreen Pin as as OUTPUT
  pinMode(8,OUTPUT);  // declare the ledred Pin as as OUTPUT
  Serial.begin(9600);       // use the serial port
}

void loop() {
  // read the sensor and store it in the variable sensorReading:
//if (sensor0Reading <=1)
 // {digitalWrite(8,HIGH);}
//else
 // {digitalWrite(8,LOW);}

 // if (sensor1Reading <=1)
 // {digitalWrite(8,HIGH);}
//else
//  {digitalWrite(8,LOW);}
 
  sensor0Reading = analogRead(knockSensor0);// if the sensor reading is greater than the threshold:
  if (sensor0Reading >= threshold) {  // toggle the status of the green led
    ledgreenState = !ledgreenState; // update the LED pin itself:
    digitalWrite(9, ledgreenState);
  // send the string "sensor0!" back to the computer, followed by newline
    Serial.println("Sensor0!");}

  sensor1Reading = analogRead(knockSensor1);
   if (sensor1Reading >= threshold) { // toggle the status of the ledPin:
    ledyellowState = !ledyellowState;// update the LED pin itself:
    digitalWrite(10, ledyellowState);
  // send the string "sensor1!" back to the computer, followed by newline
    Serial.println("Sensor1!");
  }
    delay(.5);  // delay to avoid overloading the serial port buffer
  }
