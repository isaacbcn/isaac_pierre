/*
Adafruit Arduino - Lesson 10. Simple Sounds
*/

int speakerPin = 12;

int numTones = 10;
int tones[] = {261, 277, 294, 311, 330, 349, 370, 392, 415, 440};
//            mid C  C#   D    D#   E    F    F#   G    G#   A
const int analogInPin0 = A0;
const int threshold =1;
int sensor0Value = 0; 
int output0Value = 0;   
void setup()
{
  for (int i = 0; i < numTones; i++)
  {
    tone(speakerPin, tones[i]);
    delay(500);
  }
  noTone(speakerPin);
  Serial.begin(9600);  
}

void loop()
{
  sensor0Value = analogRead(analogInPin0);
  // map it to the range of the analog out:
  output0Value = map(sensor0Value, 0, 1023, 0, 255);
   Serial.println("Sensor0!");
   for (int i = 0; i < numTones; i++)
  {
    tone(speakerPin, tones[i]);
    delay(500);
  }
  noTone(speakerPin);
}

