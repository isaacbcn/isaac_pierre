/*
  Analog input, analog output, serial output

 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.

 The circuit:
 * potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
 * LED connected from digital pin 9 to ground

 created 29 Dec. 2008
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.

 */

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin0 = A0;  // Analog input pin that sensorA is attached to
const int analogInPin1 = A1; // Analog input pin that sensorB is attached to
const int analogOutPin0 = 9; // Analog output pin that green LED is attached to
const int analogOutPin1 = 10; // Analog output pin that red the LED is attached to
const int digitalOutPin = 8; // Analog output pin that red the LED is attached to
int sensor0Value = 0;        // value read from the pot
int output0Value = 0;        // value output to the PWM (analog out)
int sensor1Value = 0;        // value read from the pot
int output1Value = 0;        // value output to the PWM (analog out)


void setup() {
  // initialize serial communications at 9600 bps:
  
  Serial.begin(9600);
}

void loop() {

  // read the analog in value:
  sensor0Value = analogRead(analogInPin0);
  // map it to the range of the analog out:
  output0Value = map(sensor0Value, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin0, output0Value);
if (output0Value >1)
  digitalWrite(8,LOW);
  else
  digitalWrite(8,HIGH);

 // read the analog in value:
sensor1Value = analogRead(analogInPin1);
 // map it to the range of the analog out:
output1Value = map(sensor1Value, 0, 1023, 0, 255);
  // change the analog out value:
analogWrite(analogOutPin1, output1Value);
//if (output1Value >1)
 // digitalWrite(8,LOW);
//  else
//  digitalWrite(8,HIGH);
  
  // print the results to the serial monitor:
  Serial.print("A = ");
  Serial.print(sensor0Value);
  Serial.print("\t outputA= ");
  Serial.println(output0Value);
  
 Serial.print("B = ");
  Serial.print(sensor1Value);
  Serial.print("\t outputA= ");
  Serial.println(output1Value);
  
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(100);
}
