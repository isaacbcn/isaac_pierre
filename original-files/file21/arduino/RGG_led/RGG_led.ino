
int redPin = 9; //set red pin to pin 9
int greenPin = 11; //set green pin to pin 11
int bluePin = 10; //set blue pin to pin 10
int brightness =100; //set brightness to 40%

void setup() {
Serial.begin(9600);
pinMode (redPin,OUTPUT); //set red to be an output
pinMode (greenPin, OUTPUT);//set green to be an output
pinMode (bluePin, OUTPUT);//set blue  to be an output

}

void loop() {
analogWrite (redPin, 0);
analogWrite (greenPin, 0);
analogWrite (bluePin, brightness);

}
