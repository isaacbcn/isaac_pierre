https://gohugo.io/variables/site/
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

Installing Hugo:

- check if you have Hugo —> go to terminal —> type: "which hugo"

- if you don’t have it, go to Hugo website and download latest version (from releases), in this case for mac 64bit.

- unzip the file

- place .exe file in /usr/local/bin <— to go there use Go from Finder menu - "go to file"

- open terminal - type "hugo version" - check if it's there

- create a new setup folder --> in terminal, go to destination folder and type: `hugo new site .` (note the dot! - represents the path to the current directory)

- from the files in created folder, open `config.toml` in text editor, here you can configure web address, language, title, and make your own prameters.

```
baseURL = "http://example.org/"
languageCode = "en-us"
title = "Hugo site / Fab Academy 2018"

[params]
description = "This is the hugo test website"
subtitle = "To learn sooo much"
author = "Isaac Pierre Racine"
```

- create anew file `index.html` in `layouts` folder
- write `{{ .Site.Title }}` - links the variable from config file with html {{}} - are hooking to Hugo configuration

- go to terminal, go to the location of the config.toml file (in the Hugo project folder) start a server by typing: `hugo sever` (ctrl c - to stop the server)

Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop

- your website is launched under the address that you’ve typed in config file

- afterwards write the preconfigured Site Variables (https://gohugo.io/variables/site/) like `.Site.Title`, and the parameters that you've specified in `config.toml` file (for example: description, subtitle, author)

```
<!DOCTYPE html>
<html>
<head>
	<title>{{ .Site.Title }}</title>
</head>
<body>

	{{ partial "home.html" . }}

	<h1>{{ .Site.Title }}</h1>
	<p>{{ .Site.Params.Description }}</p>
	<p>{{ .Site.Params.Subtitle }}</p>
	<p>{{ .Site.Params.Author }}</p>
	<hr>

	{{ partial "menu.html" . }}

</body>
</html>
```

- create `blog`folder in `content` folder

- create a file there in markdown for example `welcome.md`

- in folder `layouts`create subfolder `_default` and inside the file `single.html` — it's for creating the template for group of subsites for ex. for blog post

- create a subfolder `partials` where you are going to build all the repeatable parts which should appear on every subside (menu, home etc.)

- create a file `menu.html` and `home.html` - for building the menu part and home button

- build `menu.html`

```
<h2>Blog Menu</h2>
{{ range .Site.Menus.blog }}
	<a href="{{ .URL }}">{{ .Name }}</a><br>
{{ end }}
```

- build `home.html`

```
{{ if .IsHome }}
	You are home already
{{ else }}
	<a href="{{ .Site.BaseURL }}">Go home</a>
{{ end }}
<hr>
```

- in `index.html` file insert the above parts (menu, home)

```
<!DOCTYPE html>
<html>
<head>
	<title>{{ .Site.Title }}</title>
</head>
<body>

	{{ partial "home.html" . }}

	<h1>{{ .Site.Title }}</h1>
	<p>{{ .Site.Params.Description }}</p>
	<p>{{ .Site.Params.Subtitle }}</p>
	<p>{{ .Site.Params.Author }}</p>
	<hr>

	{{ partial "menu.html" . }}

</body>
</html>
```

- in `single.html` (template for post) insert them as well

```
<!DOCTYPE html>
<html>
<head>
	<title>{{ .Title }}</title>
	<meta name="description" content="{{ .Description }}">
</head>
<body>

	{{ partial "home.html" . }}
	{{ partial "menu.html" . }}

	{{ .Content }}

</body>
</html>
```
